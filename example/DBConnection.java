package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    final QuestionsToUser questions = new QuestionsToUser();
    String environment;

    Connection connection;
    private void setEnvironment() {
        environment = questions.getEnvironment();
    }

    public void setConnection() throws SQLException {
        setEnvironment();
        String connectionUrl = "jdbc:oracle:thin:@ldap://oraclepro-oid1.mobi.mobicorp.ch:3060/" + environment + "OLIFA1,cn=OracleContext,dc=mobi,dc=ch ldap://oraclepro-oid2.mobi.mobicorp.ch:3060/" + environment + "OLIFA1,cn=OracleContext,dc=mobi,dc=ch";
        connection = DriverManager.getConnection(connectionUrl, "AOO_LIFAEBF", environment + "AOO_LIFAEBF");
    }

    public Connection getConnection(){
        return connection;
    }
}
