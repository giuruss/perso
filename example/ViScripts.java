package org.example;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

public class ViScripts {
    String scriptMonthEol = "11";
    String scriptMonthPrivitPfMXP = "01";
    Queries queries = new Queries();
    String generatedScript;
    String contractInformation;
    Date fortschreibungBis;
    QuestionsToUser questions = new QuestionsToUser();
    Scanner scanner = new Scanner(System.in);
    DBConnection dbConnection = new DBConnection();
    String chosenYear;

    public void getViScripts() throws Exception {
        questions.askToPasteContractsNumbers();
        List contractsList = questions.getNumericDataList();
        if (null == contractsList) {
            return;
        }
        dbConnection.setConnection();
        chosenYear = questions.getViScriptYear();
        System.out.println("Show contract information ?");
        String answer = questions.getYesOrNo();
        System.out.println(" ");
        if (answer.equals("yes")) {
            getMultipleViScriptsWithContractInformation(contractsList);
        } else if (answer.equals("no")) {
            getMultipleViScriptsWithoutContractInformation(contractsList);
        }
    }

    private void getMultipleViScriptsWithContractInformation(List<String> contractsList) throws Exception {
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < contractsList.size(); i++) {
            String contractNumber = contractsList.get(i);
            if (!isDatabaseContentExisting(contractNumber)) {
                strBuilder.append("\n\ndatabase data for contract number " + contractNumber + " not found");
            } else {
                fortschreibungBis = queries.getFortschreibungBis(contractNumber, dbConnection.getConnection());
                contractInformation = queries.getContractGeneralInformation(contractNumber, dbConnection.getConnection());
                generatedScript = getViScript(contractNumber);
                strBuilder.append("\n" + contractInformation + "\nScript : " + generatedScript + "\nFortschreibung bis : " + fortschreibungBis.toString());
            }
        }
        System.out.println(strBuilder);
    }

    private void getMultipleViScriptsWithoutContractInformation(List<String> contractsList) throws Exception {
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < contractsList.size(); i++) {
            String contractNumber = contractsList.get(i);
            if (!isDatabaseContentExisting(contractNumber)) {
                strBuilder.append("database data for contract number " + contractNumber + " not found" + System.lineSeparator());
            } else {
                generatedScript = getViScript(contractNumber);
                strBuilder.append(generatedScript + System.lineSeparator());
            }
        }
        System.out.println(strBuilder);
    }

    private boolean isDatabaseContentExisting(String contractNumber) throws SQLException {
        String databaseContent = queries.getLVIDEXT(contractNumber, dbConnection.getConnection());
        if (null == databaseContent) {
            return false;
        }
        return true;
    }

    private String getViScript(String contractNumber) throws Exception {
        LocalDate convertedBeginDate;
        convertedBeginDate = getConvertedToLocalDateContractBeginDate(contractNumber);

        if (null == convertedBeginDate) {
            return null;
        }

        String contractType = queries.getContractType(contractNumber, dbConnection.getConnection()).toLowerCase();

        String scriptYear = getScriptYear();

        if (isMxsType(contractType) || isIfgType(contractType)) {
            return getMXSandIFGviScript(scriptYear, convertedBeginDate, contractNumber);
        }
        else if (!isPrivitOrIsPfOrIsMxpOrIsEol(contractType)) {
            return getNonMXSandNonIFGviScript(scriptYear, convertedBeginDate, contractNumber);
        }
        else if (isEolType(contractType)) {
            return getEOLviScript(scriptYear, contractNumber);
        }
        else if (isPRIVITorPForMXP(contractType)) {
            return getPrivitPfMXPviScript(scriptYear, contractNumber);
        }
        else {
            return "non-eligible contract : " + contractNumber + " " + queries.getContractType(contractNumber, dbConnection.getConnection());
        }
    }

    private String getMXSandIFGviScript(String currentYear, LocalDate convertedBeginDate, String contractNumber) {
        String scriptMonth = getMonthScriptString(convertedBeginDate);
        return "./doc_VInfoBatch.ksh " + java.time.LocalDate.now() + " " + currentYear + "-" + scriptMonth + "-01" + " " + contractNumber + " " + contractNumber;
    }

    private String getNonMXSandNonIFGviScript(String currentYear, LocalDate convertedBeginDate, String contractNumber) {
        LocalDate convertedBeginDateMinusOneMonth = convertedBeginDate.minusMonths(1);
        String scriptMonth = getMonthScriptString(convertedBeginDateMinusOneMonth);
        if (scriptMonth.equals("12")) {
            int scriptYear = Integer.parseInt(currentYear) - 1;
            currentYear = String.valueOf(scriptYear);
        }
        return "./doc_VInfoBatch.ksh " + java.time.LocalDate.now() + " " + currentYear + "-" + scriptMonth + "-01" + " " + contractNumber + " " + contractNumber;
    }

    private String getEOLviScript(String currentYear, String contractNumber) {
        return "./doc_VInfoBatch_Eol.ksh " + java.time.LocalDate.now() + " '" + currentYear + "-" + scriptMonthEol + "-01'" + " " + contractNumber + " " + contractNumber;
    }

    private String getPrivitPfMXPviScript(String currentYear, String contractNumber) {
        return "./doc_VInfoBatch_Mxp.ksh " + java.time.LocalDate.now() + " '" + currentYear + "-" + scriptMonthPrivitPfMXP + "-01'" + " " + contractNumber + " " + contractNumber;
    }

    private boolean isMxsType(String contractType) {
        return (contractType.contains("mxs"));
    }

    private boolean isIfgType(String contractType) {
        return (contractType.contains("ifg"));
    }

    private boolean isPrivitType(String contractType) {
        return (contractType.contains("privit"));
    }

    private boolean isPFtype(String contractType) {
        return (contractType.contains("pf"));
    }

    private boolean isMXPtype(String contractType) {
        return (contractType.contains("mxp"));
    }

    private boolean isEolType(String contractType) {
        return (contractType.contains("eol"));
    }

    private boolean isPRIVITorPForMXP(String contractType) {
        return (isPrivitType(contractType) || isPFtype(contractType) || isMXPtype(contractType));
    }

    private boolean isPrivitOrIsPfOrIsMxpOrIsEol(String contractType) {
        return (isPrivitType(contractType) || isPFtype(contractType) || isMXPtype(contractType) || isEolType(contractType));
    }

    private String getMonthScriptString(LocalDate localDate) {
        int scriptMonth = localDate.getMonthValue();
        String scriptMonthString = String.valueOf(scriptMonth);
        if (scriptMonth < 10) {
            scriptMonthString = "0" + scriptMonthString;
        }
        return scriptMonthString;
    }

    private LocalDate getConvertedToLocalDateContractBeginDate(String contractNumber) throws Exception {
        Date beginDate;
        try {
            beginDate = queries.getContractBeginDate(contractNumber, dbConnection.getConnection());
        } catch (Exception e) {
            throw new Exception("Erreur, données manquantes au sujet du contrat no " + contractNumber + " dans la base de données");
//            return null;
        }

        if (null == beginDate) {
            return null;
        }

        return beginDate.toLocalDate();
    }

    private String getScriptYear(){
        String currentYear;
        if (chosenYear.equals("last")) {
            currentYear = String.valueOf(java.time.LocalDate.now().minusYears(1).getYear());
            System.out.println(currentYear);
        } else if (chosenYear.equals("next")) {
            currentYear = String.valueOf(java.time.LocalDate.now().plusYears(1).getYear());
        } else {
            currentYear = String.valueOf(java.time.LocalDate.now().getYear());
        }
        return currentYear;
    }
}
