package org.example;

import java.io.*;

public class NDDrenaming {
    File directoryPath;
    QuestionsToUser questions = new QuestionsToUser();

    String destinationAddress;

    private void setInputDirectoryPath() {
        System.out.println("Input file");
        String path = questions.getFileAddress();
        directoryPath = new File(path);
    }

    private void setOutputDirectoryPath() {
        System.out.println("Output file");
        destinationAddress = questions.getFileAddress();
    }

    public void renameNDDfilesWithContractNumbers() throws IOException {
        setInputDirectoryPath();
        setOutputDirectoryPath();
        String vtrNr;
        String line;
        String destinationFileAddress;

        File[] filesList = directoryPath.listFiles();
        assert null != filesList;
        for (File file : filesList) {
            String filePath = file.getAbsolutePath();
            BufferedReader fileForVtr1 = new BufferedReader(new FileReader(filePath));
            vtrNr = getVtrNr(fileForVtr1);
            BufferedReader fileForVtr = new BufferedReader(new FileReader(filePath));
            StringBuilder inputBuffer = new StringBuilder();
            while ((line = fileForVtr.readLine()) != null) {
                inputBuffer.append(line);
                inputBuffer.append('\n');
            }
            vtrNr = vtrNr.replaceAll("[^0-9]", "");

            destinationFileAddress = destinationAddress + "\\" + vtrNr + ".ndd";
            FileOutputStream fileOut = new FileOutputStream(destinationFileAddress);

            fileOut.write(inputBuffer.toString().getBytes());
            fileOut.close();
            fileForVtr.close();
        }
        System.out.println("Done !");
    }

    private static String getVtrNr(BufferedReader fileForVtr) throws IOException {
        String line;

        while ((line = fileForVtr.readLine()) != null) {
            if (line.matches("^VtrNr=[0-9]+$")) {
                return line;
            }
        }
        fileForVtr.close();

        return "contract_number_not_found";
    }
}
