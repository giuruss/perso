package org.example;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class XDMformatting {
    Queries queries = new Queries();
    Scanner scanner = new Scanner(System.in);
    DBConnection dbConnection = new DBConnection();


    public void getGnummerList() throws SQLException {
        dbConnection.setConnection();
        getGeschaeftsnr();
        scanner.close();
    }

    private void getGeschaeftsnr() throws SQLException {
        StringBuilder strBuilder = new StringBuilder();
        System.out.println("Coller ci-dessous la colonne des numéros de contrats : ");
        QuestionsToUser questions = new QuestionsToUser();
        String contractNumber;
        List contractsList = questions.getNumericDataList();
        if (null != contractsList) {
            for (int i = 0; i < contractsList.size(); i++) {
                if (!contractsList.get(i).equals("0")) {
                    contractNumber = contractsList.get(i).toString();
                    String gNummer = queries.getGESCHAEFTSNR(contractNumber, dbConnection.getConnection());
                    strBuilder.append(gNummer + ", ");
                }
            }
            System.out.println(strBuilder);
        }
    }
}
