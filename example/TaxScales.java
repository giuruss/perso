package org.example;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaxScales {

    String constant2 = "2";
    String constant5 = "5";

    private String getFirstEightChars(String input) {
        if (input.length() > 8) {
            return input.substring(0, 8);
        }
        return input;
    }

    private String dateFormatting(String input) {
        return input.substring(0,4) + "-" + input.substring(4,6) + "-" + input.substring(6,8);
    }

    private String getStSatz(String input) {
        return "0." + input.substring(37);
    }

    private double getStUnterGr(String input) {
        String temp = input.substring(9, 15);
        double res = Double.parseDouble(temp);
        return res;
    }

    public List<String> getSQLscripts(List<String> inputList) {
        List<String> scriptsList = new ArrayList<>();
        if (!inputList.isEmpty()) {
            for (int i = 0; i < inputList.size(); i++) {
                String scriptDate = getFirstEightChars(inputList.get(i));
                scriptDate = dateFormatting(scriptDate);
                String stSatz = getStSatz(inputList.get(i));
                double stUnterGr = getStUnterGr(inputList.get(i));
                double stOberGr = stUnterGr + 999;
                scriptsList.add("insert into AOO_LIFAEBF.SNTSTEUERDATEN values(TO_DATE('" + scriptDate + "','yyyy-mm-dd'), "
                        + constant2 + ", " + stSatz + ", " + stUnterGr + ", " + stOberGr + ", " + constant5 +");");
            }
        }
        return scriptsList;
    }

    public void exportToFile(List<String> scriptsList, String destinationAddress) throws IOException {
        if (!scriptsList.isEmpty()) {
            String fileContent = null;

            for (int i = 0; i < scriptsList.size(); i++) {
                if (0 == i) {
                    fileContent = scriptsList.get(i);
                } else {
                    fileContent = fileContent + "\n" + scriptsList.get(i);
                }
            }

            FileOutputStream fileOut = new FileOutputStream(destinationAddress);

            fileOut.write(fileContent.getBytes());
            fileOut.close();
        }
    }

    public void scriptsProcessing() throws IOException {
        Scanner scanner = new Scanner(System.in);
        QuestionsToUser questions = new QuestionsToUser();
        TaxScales taxScales = new TaxScales();
        List<String> inputList = questions.getFormattedTaxData();
        List<String> sqlScriptsList = taxScales.getSQLscripts(inputList);
        String destinationFile = questions.getDestinationFile();
        taxScales.exportToFile(sqlScriptsList, destinationFile);
        System.out.println("The scripts have been exported to " + destinationFile + " ✅");
        scanner.close();
    }
}
