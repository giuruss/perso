package org.example;

import java.util.*;

public class ScriptsGeneration {
    QuestionsToUser questions = new QuestionsToUser();
    String today = String.valueOf(java.time.LocalDate.now());
    List contractsNumbers;

    public void ea00() {
        int year = questions.getIntYear();
        questions.askToPasteContractsNumbers();
        contractsNumbers = questions.getNumericDataList();
        for (int i = 0; i < contractsNumbers.size(); i++) {
            System.out.println("./doc_RKWbesch.ksh " + year + " '" + today + "' " + contractsNumbers.get(i) + " " + contractsNumbers.get(i));
        }
    }

    public void opp3() {
        int year = questions.getIntYear();
        questions.askToPasteContractsNumbers();
        contractsNumbers = questions.getNumericDataList();
        for (int i = 0; i < contractsNumbers.size(); i++) {
            System.out.println("./doc_Steuerbesch.ksh " + year + " '" + today + "' " + contractsNumbers.get(i) + " " + contractsNumbers.get(i));
        }
    }

    public void docpol() {
        questions.askToPasteContractsNumbers();
        contractsNumbers = questions.getNumericDataList();
        for (int i = 0; i < contractsNumbers.size(); i++) {
            System.out.println("./runBigBatch.ksh document_policy --companyId 4000 --boIdExt " + contractsNumbers.get(i));
        }
    }

    public void mxp() {
        int year = questions.getIntYear();
        questions.askToPasteContractsNumbers();
        contractsNumbers = questions.getNumericDataList();
        for (int i = 0; i < contractsNumbers.size(); i++) {
            System.out.println("./doc_VInfoBatch_Mxp.ksh '" + today + "' '" + year + "-01-01' " + contractsNumbers.get(i) + " " + contractsNumbers.get(i));
        }
    }

    public void lfimexLoad() {
        questions.askToPasteContractsNumbers();
        contractsNumbers = questions.getNumericDataList();
        for (int i = 0; i < contractsNumbers.size(); i++) {
            System.out.println("./lfimex_srv.ksh -a load -m 4000 -x " + contractsNumbers.get(i) + " -f " + contractsNumbers.get(i) + ".lfimex");
        }
    }

    public void lfimexSave() {
        questions.askToPasteContractsNumbers();
        contractsNumbers = questions.getNumericDataList();
        for (int i = 0; i < contractsNumbers.size(); i++) {
            System.out.println("./lfimex_srv.ksh -a save -m 4000 -x " + contractsNumbers.get(i) + " -f " + contractsNumbers.get(i) + ".lfimex");
        }
    }

    public void lfimexDelete() {
        questions.askToPasteContractsNumbers();
        contractsNumbers = questions.getNumericDataList();
        for (int i = 0; i < contractsNumbers.size(); i++) {
            System.out.println("./lfimex_srv.ksh -a delete -m 4000 -x " + contractsNumbers.get(i));
        }
    }

    public void columnsToLine() {
        questions.askToPasteContractsNumbers();
        contractsNumbers = questions.getNumericDataList();
        int lastContract = contractsNumbers.size() - 1;
        String oneLine = null;
        for (int i = 0; i < contractsNumbers.size(); i++) {
            if (0 == i) {
                oneLine = contractsNumbers.get(i) + ", ";
            } else if (lastContract == i) {
                oneLine = oneLine + contractsNumbers.get(i);
            } else {
                oneLine = oneLine + contractsNumbers.get(i) + ", ";
            }
        }
        System.out.println(oneLine);
    }

    private String returnColumnsToLine() {
        questions.askToPasteIds();
        contractsNumbers = questions.getNumericDataList();
        int lastContract = contractsNumbers.size() - 1;
        String oneLine = null;
        for (int i = 0; i < contractsNumbers.size(); i++) {
            if (0 == i) {
                oneLine = contractsNumbers.get(i) + ", ";
            } else if (lastContract == i) {
                oneLine = oneLine + contractsNumbers.get(i);
            } else {
                oneLine = oneLine + contractsNumbers.get(i) + ", ";
            }
        }
        return oneLine;
    }

    public void lineToColumn() {
        String line = questions.getLine();
        line = line.replace(",", "\n");
        line = line.replace(" ", "");
        System.out.println();
        System.out.print(line);
        System.out.println();
    }

    public void updateTbbjob_entryNDDstatusTo9() {
        String jobIdNumbersInLine = returnColumnsToLine();
        System.out.println();
        System.out.println("update AOO_LIFAbig.tbbjob_entry\n" +
                "set status = 9\n" +
                "where JOB_ID in (" + jobIdNumbersInLine + ")\n" +
                "and status = 0\n");
    }

    public void updateOutbandDocumentPolicyStatusToNull() {
        String pkIdNumbersInLine = returnColumnsToLine();
        System.out.println();
        System.out.println("update AOO_LIFAEBF.IFCOUTBOUNDDATA \n" +
                "set PROCESSDATETIME=NULL,PROCESSINGSTATUSCV=0,BATCHJOBID=NULL \n" +
                "where COMPANYID = 4000\n" +
                "--and PROCESSINGSTATUSCV in (8, 16)\n" +
                "and PK in (" + pkIdNumbersInLine + ")\n");
    }



    public void removeDuplicates() {
        questions.askToPasteContractsNumbers();
        List list = questions.getNumericDataList();
        List newList = new ArrayList();
        if (null != list) {
            for (int i = 0; i < list.size(); i++) {
                if (!newList.contains(list.get(i))) {
                    newList.add(list.get(i));
                }
            }
            System.out.println("Numbers without duplicates : ");
            newList.forEach(System.out::println);
        }
    }

    public void compare2Lists() {
        QuestionsToUser questionFirstList = new QuestionsToUser();
        QuestionsToUser questionSecondList = new QuestionsToUser();
        System.out.println("First list : ");
        List firstList;
        List secondList;
        List differencesFirstList;
        List differencesSecondList;
        List concordanceList;
        firstList = questionFirstList.getNumericDataList();
        if (null != firstList) {
            if (firstList.isEmpty()) {
                System.out.println("First list is empty, program stopped");
            } else {
                System.out.println("Second list : ");
                secondList = questionSecondList.getNumericDataList();
                if (null != secondList) {
                    if (secondList.isEmpty()) {
                        System.out.println("Second list is empty, program stopped");
                    } else {
                        if (null != secondList) {
                            differencesFirstList = getNumbersThatAreInTheFirstListAndNotInTheSecond(firstList, secondList);
                            System.out.println();
                            if (differencesFirstList.isEmpty()) {
                                System.out.println("All numbers of the first list are in the second list as well");
                            } else {
                                System.out.println("Numbers that are in the first list but not in the second list : ");
                                differencesFirstList.forEach(System.out::println);
                            }

                            differencesSecondList = getNumbersThatAreInTheFirstListAndNotInTheSecond(secondList, firstList);
                            System.out.println();
                            if (differencesSecondList.isEmpty()) {
                                System.out.println("All numbers of the second list are in the first list as well");
                            } else {
                                System.out.println("Numbers that are in the second list but not in the first list : ");
                                differencesSecondList.forEach(System.out::println);
                            }

                            concordanceList = getConcordancesBetweenFirstAndSecondList(firstList, secondList);
                            System.out.println();
                            if (concordanceList.isEmpty()) {
                                System.out.println("There are no matching numbers between the two lists");
                            } else {
                                if (!differencesFirstList.isEmpty() || !differencesSecondList.isEmpty()) {
                                    System.out.println("Numbers that are in both the first and second lists : ");
                                    concordanceList.forEach(System.out::println);
                                } else {
                                    System.out.println("First and second list are identical !");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private List getNumbersThatAreInTheFirstListAndNotInTheSecond(List firstList, List secondList) {
        List differences = new ArrayList();
        for (int i = 0; i < firstList.size(); i++) {
            if (!secondList.contains(firstList.get(i))) {
                differences.add(firstList.get(i));
            }
        }
        return differences;
    }

    private List getConcordancesBetweenFirstAndSecondList(List firstList, List secondList) {
        List concordances = new ArrayList();
        for (int i = 0; i < firstList.size(); i++) {
            if (secondList.contains(firstList.get(i))) {
                concordances.add(firstList.get(i));
            }
        }
        return concordances;
    }


    public void showDuplicates() {

//        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 2, 3, 4, 5, 6, 5);
        List<Integer> numbers = new ArrayList<>();
        questions.askToPasteContractsNumbers();
        List list = questions.getNumericDataList();

        for (int i = 0; i < list.size(); i++) {
            int val = Integer.parseInt((String) list.get(i));
            numbers.add(val);
        }

        // Utilisation d'un dictionnaire pour stocker les nombres et leur nombre d'occurrences
        Map<Integer, Integer> occurrences = new HashMap<>();

        // Boucle sur la liste pour compter les occurrences de chaque nombre
        for (int num : numbers) {
            if (occurrences.containsKey(num)) {
                occurrences.put(num, occurrences.get(num) + 1);
            } else {
                occurrences.put(num, 1);
            }
        }

        // Boucle sur les entrées du dictionnaire pour afficher les nombres qui apparaissent au moins deux fois
        for (Map.Entry<Integer, Integer> entry : occurrences.entrySet()) {
            if (entry.getValue() >= 2) {
                System.out.println("Number " + entry.getKey() + " is appearing " + entry.getValue() + " times.");
            }
        }
    }

    public void showDuplicatesOld() {
        questions.askToPasteContractsNumbers();
        List list = questions.getNumericDataList();
        List cloneList = list;
        boolean isDuplicatedValueExisting = false;
        if (null != list) {
            int counter = 0;
            for (int i = 0; i < list.size(); i++) {
                for (int j = 0; j < cloneList.size(); j++) {
                    if (cloneList.get(j).equals(list.get(i))) {
                        counter++;
                    }
                }
                if (counter > 1) {
                    if (!isDuplicatedValueExisting) {
                        isDuplicatedValueExisting = true;
                    }
                    System.out.println(list.get(i) + " is appearing " + counter + " times.");
                    cloneList.remove(list.get(i));
                }
                counter = 0;
            }
        }
        if (!isDuplicatedValueExisting) {
            System.out.println("There is no duplicated value in the list");
        }
    }



}


