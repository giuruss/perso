package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuestionsToUser {
    Scanner scanner;
    DataCheck dataCheck;
    List numericDataList;
    List nonNumericDataList;
    String data;
    String choice;

    public QuestionsToUser() {
        scanner = new Scanner(System.in);
        dataCheck = new DataCheck();
        numericDataList = new ArrayList();
        nonNumericDataList = new ArrayList();
    }

    public String getEnvironment() {
        String environment;

        do {
            System.out.print("Environnement : ");
            environment = scanner.nextLine().toUpperCase();
        }
        while (!(environment.equals("B") ||
                environment.equals("W") ||
                environment.equals("V") ||
                environment.equals("I") ||
                environment.equals("T") ||
                environment.equals("Z") ||
                environment.equals("Y")));

        return environment;
    }

    public String getYesOrNo() {
        System.out.print("Type yes or no : ");
        String answer = scanner.nextLine().toLowerCase();
        while (!(answer.equals("yes") || answer.equals("no"))) {
            System.out.print("Type yes or no : ");
            answer = scanner.nextLine().toLowerCase();
        }
        return answer;
    }

    public String getViScriptYear() {
        String viScriptYear;
        do {
            System.out.println("VI year selection, for current year just press Enter");
            System.out.println("Type 'last' if VI value date must be last year");
            System.out.println("Type 'next' if VI value date must be next year");
            viScriptYear = scanner.nextLine();
        }
        while(!viScriptYear.isEmpty() && !viScriptYear.equals("last") && !viScriptYear.equals("next"));

        return viScriptYear;
    }

    public ArrayList<String> listFulfilling() {
        ArrayList<String> myList = new ArrayList<>();
        while (scanner.hasNextLine() && !scanner.nextLine().equals("end")) {
            myList.add(scanner.nextLine());
        }

        for (String i : myList) {
            boolean res = true;
            try {
                Integer.parseInt(i);
            } catch (NumberFormatException e) {
                System.out.println("Invalid contract number");
                res = false;
            }
            if (!res){
                return null;
            }
        }
        return myList;
    }

    public List getNumericDataList() {
        do {
            data = dataCheck.getData(scanner);
            numericAndNonNumericDataListsUpdate();
        }
        while(!dataCheck.isDataZero(data));
        if (isNonNumericDataExisting()) {
            numericDataList.removeAll(numericDataList);
            dataCheck.invalidDataListMessage(nonNumericDataList);
            scanner.close();
            return null;
        }
        return numericDataList;
    }

    public void getFormattedNumbersList() {
        askToPasteContractsNumbers();
        do {
            data = dataCheck.getData(scanner);
            numericAndNonNumericDataListsUpdate();
        }
        while(!dataCheck.isDataZero(data));
        if (isNonNumericDataExisting()) {
            numericDataList.removeAll(numericDataList);
            dataCheck.invalidDataListMessage(nonNumericDataList);
            scanner.close();
            System.out.println("List contains non numerical values");
        }
        if (!isNonNumericDataExisting()) {
            numericDataList.forEach(System.out::println);
        }
    }

    private boolean isNonNumericDataExisting() {
        return !nonNumericDataList.isEmpty();
    }

    private void numericAndNonNumericDataListsUpdate() {
        if (!dataCheck.isDataZero(data)) {
            if (dataCheck.isDoubleNumeric(data)) {
                numericDataList.add(data);
            } else {
                nonNumericDataList.add((data));
            }
        }
    }

    public List<String> getFormattedTaxData() {
        String data;
        String processedLine = null;
        List myList = new ArrayList<>();

        System.out.println("Paste here the content of the file and type '0' and 'Enter' at the end \n");
        do {
            data = dataCheck.formatTaxData(scanner.nextLine());

            if (null != data) {
                processedLine = data;

                if (!dataCheck.isDoubleNumeric(processedLine)) {
                    myList.removeAll(myList);
                    dataCheck.invalidDataMessage(processedLine);
//                    scanner.nextLine();
                    scanner.close();
                    break;
                }

                if (!dataCheck.isDataZero(processedLine)) {
                    myList.add(processedLine);
                }
            }
        }
        while(!dataCheck.isDataZero(processedLine));
        return myList;
    }

    public String getDestinationFile() {
        String fileAddress = getFileAddress();
        String fileName = getSqlFileName();
        return fileAddress + "\\" + fileName;
    }

    public String getFileAddress() {
        System.out.print("File address : ");
        return scanner.nextLine();
    }

    private String getSqlFileName() {
        System.out.print("File name : ");
        return scanner.nextLine() + ".sql";
    }

    public int getProgramChoice() {
        do {
            System.out.print("0. End the program\n1. VI\n2. XDM\n3. Taxes at source\n4. Format numbers" +
                    "\n5. Import funds prices\n6. Get Fortschreibung scripts\n7. NDD rename\n8. EA00 (./doc_RKWbesch.ksh | Vorlage : M:\\RKWbesch.dot)\n" +
                    "9. OPP3 (./doc_Steuerbesch.ksh | Vorlage : M:\\Steuerbesch.dot)\n10. DOCPOL\n11. MXP (./doc_VInfoBatch_Mxp.ksh | Vorlage : M:\\VInfoBatchMXP.dot)\n12. lfimexLoad\n13. lfimexSave\n14. lfimexDelete\n15. Columns to line\n" +
                    "16. Line to column\n17. Update NDD\n18. Update document policy\n19. Show duplicates\n20. Remove duplicates\n" +
                    "21. Compare 2 lists\n");
            System.out.print("choice : ");
            choice = scanner.nextLine();
        }
        while (!dataCheck.isDoubleNumeric(choice));
        return Integer.parseInt(choice);
    }

//    public int getChoiceToContinueOrNot() {
//        int intchoice;
//        do {
//            System.out.print("0. End the program\n2. Continue\n");
//            System.out.print("choice : ");
//            choice = scanner.nextLine();
//
//            intchoice = Integer.parseInt(choice);
//
//            if (0 != intchoice) {
//                intchoice = getProgramChoice();
//                try {
//                    choiceProcessing(intchoice);
//                } catch (Exception e) {
//                    throw new RuntimeException(e);
//                }
//            }
//        }
//        while (!dataCheck.isDoubleNumeric(choice));
//        return Integer.parseInt(choice);
//    }

    public void choiceProcessing() throws Exception {
        int choice = getProgramChoice();
//        String choiceToContinueOrNot;
//        do {
        switch (choice) {
            case 0:
                System.out.println("Program finished");
                break;
            case 1:
                ViScripts viScripts = new ViScripts();
                viScripts.getViScripts();
                break;
            case 2:
                XDMformatting xdMformatting = new XDMformatting();
                xdMformatting.getGnummerList();
                break;
            case 3:
                TaxScales taxScales = new TaxScales();
                taxScales.scriptsProcessing();
                break;
            case 4:
                getFormattedNumbersList();
                break;
            case 5:
                FundsPricesImport fundsPricesImport = new FundsPricesImport();
                fundsPricesImport.getFundsPrices();
                break;
            case 6:
                Fortschreibung fortschreibung = new Fortschreibung();
                fortschreibung.getFortschreibungScripts();
                break;
            case 7:
                NDDrenaming NDDrenaming = new NDDrenaming();
                NDDrenaming.renameNDDfilesWithContractNumbers();
                break;
            case 8:
                ScriptsGeneration scriptsGeneratioEA00 = new ScriptsGeneration();
                scriptsGeneratioEA00.ea00();
                break;
            case 9:
                ScriptsGeneration scriptsGenerationOPP3 = new ScriptsGeneration();
                scriptsGenerationOPP3.opp3();
                break;
            case 10:
                ScriptsGeneration scriptsGenerationDOCPOL = new ScriptsGeneration();
                scriptsGenerationDOCPOL.docpol();
                break;
            case 11:
                ScriptsGeneration scriptsGenerationMXP = new ScriptsGeneration();
                scriptsGenerationMXP.mxp();
                break;
            case 12:
                ScriptsGeneration scriptsGenerationLFIMEXload = new ScriptsGeneration();
                scriptsGenerationLFIMEXload.lfimexLoad();
                break;
            case 13:
                ScriptsGeneration scriptsGenerationLFIMEXsave = new ScriptsGeneration();
                scriptsGenerationLFIMEXsave.lfimexSave();
                break;
            case 14:
                ScriptsGeneration scriptsGenerationLFIMEXdelete = new ScriptsGeneration();
                scriptsGenerationLFIMEXdelete.lfimexDelete();
                break;
            case 15:
                ScriptsGeneration scriptsGenerationColumnsToLine = new ScriptsGeneration();
                scriptsGenerationColumnsToLine.columnsToLine();
                break;
            case 16:
                ScriptsGeneration scriptsGenerationLineToColumn = new ScriptsGeneration();
                scriptsGenerationLineToColumn.lineToColumn();
                break;
            case 17:
                ScriptsGeneration scriptsGenerationNDD = new ScriptsGeneration();
                scriptsGenerationNDD.updateTbbjob_entryNDDstatusTo9();
                break;
            case 18:
                ScriptsGeneration scriptsGenerationDocumentPolicy = new ScriptsGeneration();
                scriptsGenerationDocumentPolicy.updateOutbandDocumentPolicyStatusToNull();
                break;
            case 19:
                ScriptsGeneration scriptsGenerationShowDuplicates = new ScriptsGeneration();
                scriptsGenerationShowDuplicates.showDuplicates();
                break;
            case 20:
                ScriptsGeneration scriptsGenerationRemoveDuplicates = new ScriptsGeneration();
                scriptsGenerationRemoveDuplicates.removeDuplicates();
                break;
            case 21:
                ScriptsGeneration scriptsGenerationCompare2Lists = new ScriptsGeneration();
                scriptsGenerationCompare2Lists.compare2Lists();
                break;
            default:
                System.out.println("Invalid choice, program stopped");
                break;
        }
//            if (0 != choice) {
//                System.out.println();
//                System.out.println("Continue program ? Type yes to continue or anything else to stop");
//                choiceToContinueOrNot = scanner.nextLine();
//                if (choiceToContinueOrNot.equals("yes")) {
//                    choice = getProgramChoice();
//                } else {
//                    break;
//                }
//            }
//        }
//        while (0 != choice);
    }

    public void askToPasteContractsNumbers() {
        System.out.println("Paste the contract numbers one under the other, type '0' then 'Enter' to finish : ");
    }

    public void askToPasteIds() {
        System.out.println("Paste the job id(s) ok the PK numbers one under the other, type '0' then 'Enter' to finish : ");
    }

    public int getIntMonthNumber() {
        String stringMonth;
        int intMonth;
        do {
            stringMonth = getStringMonthNumber();
            intMonth = Integer.parseInt(stringMonth);
        }
        while (intMonth < 1 || intMonth > 12);
        return intMonth;
    }

    public int getIntYear() {
        String stringYear;
        int intYear;
        do {
            stringYear = getStringYear();
            intYear = Integer.parseInt(stringYear);
        }
        while (intYear < 1900 || intYear > 2200);
        return intYear;
    }

    private String getStringMonthNumber() {
        String stringMonth;
        do {
            System.out.print("Type month number between 1 and 12 : ");
            stringMonth = scanner.nextLine();
        }
        while (!dataCheck.isIntNumeric(stringMonth));
        return stringMonth;
    }

    private String getStringYear() {
        String stringYear;
        do {
            System.out.print("Type year between 1900 and 2200 : ");
            stringYear = scanner.nextLine();
        }
        while (!dataCheck.isIntNumeric(stringYear));
        return stringYear;
    }

    public String getLine() {
        System.out.print("Type line : ");
        return scanner.nextLine();
    }
}
