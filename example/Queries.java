package org.example;

import java.sql.*;

public class Queries {
    public String getGESCHAEFTSNR(String contractNumber, Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rsShort = stmt.executeQuery("SELECT LV.C_GESCHAEFTSNR, JUR.LVIDEXT FROM AOO_LIFAEBF.LV LV,AOO_LIFAEBF.JURLV JUR WHERE LV.LVID=JUR.LVID AND LV.BEARBIDABG=JUR.BEARBIDABG AND LV.BEARBIDABG = 999999 AND JUR.LVIDEXT in (" + contractNumber + ")");
        rsShort.next();
        try {
            String geschaeftsnr = rsShort.getString("C_GESCHAEFTSNR");
            String lvidext = rsShort.getString("LVIDEXT");

            if (null == geschaeftsnr) {
                return lvidext;
            } else if (null == lvidext) {
                return null;
            }
            return geschaeftsnr;
        } catch (SQLException e) {
//            throw new SQLException(e.getCause());
            return "Contract " + contractNumber + " not found in the database";
        }
    }

    public String getLVIDEXT(String contractNumber, Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rsShort = stmt.executeQuery("SELECT LVIDEXT,LV.LVID,LV.LVBEGT,LV.LVJAHRTAG,LV.C_COR_PROD,LV.C_GESCHAEFTSNR,LV.C_UNUMMER,LV.PDID,LV.LVSTATUS,LV.LVABLT FROM AOO_LIFAEBF.LV LV, AOO_LIFAEBF.JURLV WHERE LV.LVID= AOO_LIFAEBF.JURLV.LVID AND LV.BEARBIDABG=AOO_LIFAEBF.JURLV.BEARBIDABG AND LV.BEARBIDABG = 999999 AND AOO_LIFAEBF.JURLV.LVIDEXT in ("+ contractNumber +")");
        rsShort.next();

        try {
            return "\nContract number : " + rsShort.getString("LVIDEXT");
        } catch (SQLException e) {
            return null;
        }
    }

    public String getContractGeneralInformation(String contractNumber, Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rsShort = stmt.executeQuery("SELECT LVIDEXT,LV.LVID,LV.LVBEGT,LV.LVJAHRTAG,LV.C_COR_PROD,LV.C_GESCHAEFTSNR,LV.C_UNUMMER,LV.PDID,LV.LVSTATUS,LV.LVABLT FROM AOO_LIFAEBF.LV LV, AOO_LIFAEBF.JURLV WHERE LV.LVID= AOO_LIFAEBF.JURLV.LVID AND LV.BEARBIDABG=AOO_LIFAEBF.JURLV.BEARBIDABG AND LV.BEARBIDABG = 999999 AND AOO_LIFAEBF.JURLV.LVIDEXT in ("+ contractNumber +")");
        rsShort.next();

        try {
            return "\nStatut : " + rsShort.getString("LVSTATUS") + "\nContract number : " + rsShort.getString("LVIDEXT") + "\nC_GESCHAEFTSNR : " + rsShort.getString("C_GESCHAEFTSNR") + "\nContract type : " + rsShort.getString("C_COR_PROD") + "\nContract begin date : " + rsShort.getString("LVBEGT");
        } catch (SQLException e) {
            throw new SQLException(e.getCause());
        }
    }

    public String getContractType(String contractNumber, Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT LVIDEXT,LV.LVID,LV.LVBEGT,LV.LVJAHRTAG,LV.C_COR_PROD,LV.C_GESCHAEFTSNR,LV.C_UNUMMER,LV.PDID,LV.LVSTATUS,LV.LVABLT FROM AOO_LIFAEBF.LV LV, AOO_LIFAEBF.JURLV WHERE LV.LVID= AOO_LIFAEBF.JURLV.LVID AND LV.BEARBIDABG=AOO_LIFAEBF.JURLV.BEARBIDABG AND LV.BEARBIDABG = 999999 AND AOO_LIFAEBF.JURLV.LVIDEXT in ("+ contractNumber +")");
        rs.next();
        try {
            return rs.getString("C_COR_PROD");
        } catch (SQLException e) {
            throw new SQLException(e.getCause());
        }
    }

    public Date getContractBeginDate(String contractNumber, Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT LVIDEXT,LV.LVID,LV.LVBEGT,LV.LVJAHRTAG,LV.C_COR_PROD,LV.C_GESCHAEFTSNR,LV.C_UNUMMER,LV.PDID,LV.LVSTATUS,LV.LVABLT FROM AOO_LIFAEBF.LV LV, AOO_LIFAEBF.JURLV WHERE LV.LVID= AOO_LIFAEBF.JURLV.LVID AND LV.BEARBIDABG=AOO_LIFAEBF.JURLV.BEARBIDABG AND LV.BEARBIDABG = 999999 AND AOO_LIFAEBF.JURLV.LVIDEXT in ("+ contractNumber +")");
        rs.next();
        try {
            return rs.getDate("LVBEGT");
        } catch (SQLException e) {
            throw new SQLException(e.getCause());
        }
    }

    public Date getFortschreibungBis(String contractNumber, Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rsShort = stmt.executeQuery("SELECT DISTINCT JUR.LVIDEXT, LVS.FORTBIS, LV.C_COR_PROD FROM AOO_LIFAEBF.JURLV JUR, AOO_LIFAEBF.LVSTAND LVS, AOO_LIFAEBF.LV LV WHERE JUR.LVID=LVS.LVID AND LVS.LVID=LV.LVID AND LVS.BEARBIDABG = 999999 and JUR.LVIDEXT in (" + contractNumber + ")");
        rsShort.next();

        try {
//            return "\nFortschreibung bis : " + rsShort.getDate("FORTBIS");
            return rsShort.getDate("FORTBIS");
        } catch (SQLException e) {
            throw new SQLException(e.getCause());
        }
    }

}
