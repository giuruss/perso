package org.example;

public class FundsPricesImport {

    final int MAXDECIMALVALUE = 99998;
    final int MAXUNITVALUE = 2500;

    public void getFundsPrices() {
        QuestionsToUser questions = new QuestionsToUser();
        Utils utils = new Utils();
        String stringMonthNumber = utils.formatStringMonthGiving2Digits(questions.getIntMonthNumber());
        int year = questions.getIntYear();

        System.out.println("ISIN;GUELTIG_BIS;RUECKG_KURS;WAEHRUNG_SKZ\n" +
                "CH0484059172;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "DE000A3KS5Y7;"  + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0161534606;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "CH1127510498;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "CH0211608895;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU1203844490;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "CH0484059198;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0994951381;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0161539233;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU1495639897;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0112806921;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "CH0203515074;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0112806418;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0161539076;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0044849320;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "DE000A3GWZE2;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "CH0011900070;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "CH0016757517;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU1203844060;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0161535165;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0161533624;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU1203843922;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0033502740;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0010001369;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "LU0026741651;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "IE00B3KRGG97;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "IE0002987190;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "CH0484059214;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "CH0016431709;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n" +
                "DE000A3K5HZ0;" + year + stringMonthNumber + "01;" + utils.getRandomNumber(1,MAXUNITVALUE) + "," + utils.getRandomNumber(0,MAXDECIMALVALUE) + ";7\n");
    }
}
