package org.example;

import java.util.List;
import java.util.Scanner;

public class DataCheck {
    public boolean isDoubleNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public boolean isIntNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private String contractNumberFormat(String temp) {
        temp = temp.replace(",","");
        temp = temp.replace("-","");
        temp = temp.replace(".","");
        return temp;
    }

    private String removeSpaces(String temp) {
        if (temp != null) {
            return temp.replace(" ", "");
        }
        return null;
    }

    private String removeFirstBlock(String line) {
        if (line != null) {
            return line.replace("0600VDI", "");
        }
        return null;
    }

    private String removeJKlines(String line) {
        if (line.contains("0600VDJ") || line.contains("0600VDK")) {
            return null;
        }
        return line;
    }

    public String getData(Scanner scanner) {
        return contractNumberFormat(scanner.nextLine());
    }

    public String formatTaxData(String data) {
        String line = removeJKlines(data);
        line = removeFirstBlock(line);

        if (line != null) {
            return removeSpaces(line);
        }
        return line;
    }

    public boolean isDataZero(String data) {
        return data.equals("0");
    }

    public void invalidDataMessage(String data) {
        System.out.println("\n'" + data + "' is an invalid data, program stopped");
    }

    public void invalidDataListMessage(List dataList) {
        dataList.forEach(System.out::println);
        System.out.println("\n Lines above contain non-numeric values, so program has been stopped\n");
    }
}
