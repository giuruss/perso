package org.example;

import java.util.concurrent.ThreadLocalRandom;

public class Utils {
    public int getRandomNumber(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    public String formatStringMonthGiving2Digits(int intMonthNumber) {
        if (intMonthNumber < 10) {
            return "0" + intMonthNumber;
        }
        return String.valueOf(intMonthNumber);
    }
}
