package org.example;

import java.util.List;

public class Fortschreibung {

    public void getFortschreibungScripts() {
        QuestionsToUser questions = new QuestionsToUser();
        questions.askToPasteContractsNumbers();
        List contractsList = questions.getNumericDataList();
        Utils utils = new Utils();
        int year = questions.getIntYear();
        String month = utils.formatStringMonthGiving2Digits(questions.getIntMonthNumber());

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < contractsList.size(); i++) {
            String contractNumber = contractsList.get(i).toString();
            strBuilder.append("./runLcBatch.ksh periodicalProcessing -m 4000 -y " + java.time.LocalDate.now() + " -t 01." + month + "." + year + " -x " + contractNumber + System.lineSeparator());
        }
        System.out.println(strBuilder);
    }
}
